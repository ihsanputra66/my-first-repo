# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-14 09:25
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0003_auto_20171011_2211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 14, 16, 24, 59, 891317, tzinfo=utc)),
        ),
    ]
