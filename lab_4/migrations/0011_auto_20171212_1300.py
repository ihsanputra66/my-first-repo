# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 06:00
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0010_auto_20171122_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 12, 13, 0, 55, 650486, tzinfo=utc)),
        ),
    ]
