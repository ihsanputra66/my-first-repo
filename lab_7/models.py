from django.db import models

class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250)
    alamat = models.CharField(max_length=250, null=True)
    added_at = models.DateField(auto_now_add=True)

# Create your models here.
