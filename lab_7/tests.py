from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, delete_friend, model_to_dict
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper


class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        new_activity = Friend.objects.create(
            friend_name='putra', npm='1000000')
        delete_friend(self, new_activity.pk)
        counting_all_available_todo = Friend.objects.all().count()
        self.assertEqual(counting_all_available_todo, 0)

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "putra", 'npm': "10000"}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(
            auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken': False})

    def test_model_to_dict(self):
        new_activity = Friend.objects.create(
            friend_name='Putra', npm='1000000')
        data = model_to_dict(new_activity)
        self.assertEqual
        ('{"npm": "0606101452", "added_at": "2017-11-15","friend_name": "HUGO REYNALDO"}', data)
        # Create your tests here.

    def test_csui_helper_cannot_login(self):
        csui = CSUIhelper()
        csui.instance.set_username_wrong()
        with self.assertRaises(Exception):
            csui_helper.instance.get_access_token()

    def test_invalid_sso_raise_exception(self):
        CSUI = CSUIhelper.instance
        with self.assertRaises(Exception) as context:
            CSUI.get_access_token()
        self.assertIn("username atau password sso salah",
                      str(context.exception))

