1
    1) daftarkan dan masuk ke https://developers.facebook.com/ dan login, kemudian buat aplikasi untuk web
    2) Dari API yang dibaca, pertama2 setelah buat app akan disuruh copy kode fbAsyncInit yang
    berfungsi untuk mendefinisikan app id kita dan API version. Selain itu, juga function yang
    berfungsi untuk menghubungkan javascript buatan kita dengan API facebook. Setelah itu,
    tombol di lab_8 html yang akan memanggil fungsi facebookLogin yang kemudian memanggil FB.api
    untuk mengakses method API facebook dan melakukan operasi login dengan scope tertentu. Scope
    ini nantinya digunakan sebagai permission ketika kita membutuhkan akses, seperti kepada
    profile facebook, user post, dll
    3) Menggunakan fungsi render yang ada di javascript dan mengecek apakah kondisi loginFlag
    itu true, yaitu ketika status user sudah login, kemudian memanggil method getUserData
    yang meneruma parameter berupa user, data user didapat dari response, yang didapat dari
    method getUserData yang ada di luar method render, method getUserData yang di luar itu
    sendiri berfungsi untuk memanggil API facebook untuk meminta fields yang diperlukan seperti
    id, nama, about, email, gender, dll. setelah itu, baru method tersebut bisa dipanggil dari
    method render yang menampilkan data-data ke file html
    4) Dari html yang sudah dirender ketika user sudah login, terdapat button yang ketika diklik
    akan memanggil method postStatus, dari method postStatus akan mengambil nilai dari textarea
    yang berisi status kita, kemudian akan mengakses method API facebook dengan parameter message
    berisi status yang akan dipost, setelah itu akan merender kembali untuk menampilkan post ter-
    baru
    5) Dipanggil methodnya di method render, yang namanya getUserPosts, dari method tersebut akan
    memanggil API facebook dan meminta daftar post yang ada di facebook tersebut
    6) Memanggil method facebookLogout yang akan memanggil API facebook untuk me-logout user ter-
    sebut. Setelah itu dilakukan render yang loginFlagnya berisi false, yang menandakan user sudah
    logout
    7) Menambahkan css seperti warna container untuk user post, user profile dan menempatkannya di
    posisi yang bagus. Selain itu, untuk tombol digunakan bootstrap supaya mudah diimplementasikan

2
    1) mengambil dari lab_1
    2) menambahkan test untuk fungsi index yang mengetes apakah fungsi index berjalan dengan baik
