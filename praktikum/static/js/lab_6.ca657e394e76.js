// Calculator
var print = document.getElementById('print');
var erase = false;
var temaBaru = "";

var go = function(x) {
  if (x === 'ac') {
      print.value = ""
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x == 'sin'){
    print.value = Math.round(Math.sin(evil(print.value)*Math.PI/180)*10000 )/10000;

  } else if (x == 'tan'){
    print.value = Math.round(evil(Math.tan(print.value))*10000)/10000;

  } 
  else if(x == 'log'){
    print.value = Math.round(evil(Math.log(print.value)*10000))/10000;

  }
  else {
    if(print.value === "0" && x !== "*" && x !== " / " && x !== ".")
      print.value = x;
    else
      print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

var themes = [    
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]
localStorage.setItem("themes", JSON.stringify(themes));
localStorage.setItem("selectedTheme", JSON.stringify({"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}));


$("#hide-chat").click(function() {
  $(".chat-body").slideToggle();
});

$('body').css({"backgroundColor" : JSON.parse(localStorage.selectedTheme).Indigo.bcgColor});
$('.text-center').css({"color": JSON.parse(localStorage.selectedTheme).Indigo.fontColor});

$(document).ready(function(){
  
  $('.my-select').select2({
    data: JSON.parse(localStorage.getItem("themes"))
  });

   $('.apply-button').on('click', function(){  // sesuaikan class button
     // [TODO] ambil value dari elemen select .my-select
     temaBaru = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
     // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
     $('body').css({"backgroundColor" : temaBaru['bcgColor']});
     // [TODO] ambil object theme yang dipilih
     $('.text-center').css({"color": temaBaru['fontColor']});
     // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
     localStorage.setItem('selectedTheme', JSON.stringify(temaBaru));
     // [TODO] simpan object theme tadi ke local storage selectedTheme
   });

   $('textarea').keypress(function(event){
     if(event.which == 13){
       var insert = $('.msg-insert').html();
       var chatbox = $('textarea').val();
       if(chatbox.length == 0){
       }
       else{
         $('.msg-insert').html(insert+'<p class="msg-send">'+ chatbox+'</p>');
         $('textarea').val("");
        }
        event.preventDefault();
      
      }
    });
  
});





